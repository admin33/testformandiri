﻿using Microsoft.AspNetCore.Identity;

namespace BackEnd.Data.Models;

public class AppRole : IdentityRole<int>
{
}